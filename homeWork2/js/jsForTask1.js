var fruits = ["яблоко", "груша", "слива", "апельсин"];
var moreFruits = ["киви", "лайм"];
//Join
var changedWithJoin = fruits.join("<br>");
document.write("Использован метод join: <br>" + changedWithJoin);
//Concat
var allFruits = fruits.concat(moreFruits);
document.write("<br><br>Использован метод concat: <br>" + allFruits);
//Reverse
allFruits = allFruits.reverse();
document.write("<br><br>Использован метод reverse: <br>" + allFruits);
//Slice
var someFruits = allFruits.slice(3, 5);
document.write("<br><br>Использован метод slice: <br>" + someFruits);
//Splice
allFruits.splice(1, 0, "банан");
document.write("<br><br>Добавлен элемент методом splice: <br>" + allFruits);
allFruits.splice(3, 2);
document.write("<br>Удалены элементы методом splice: <br>" + allFruits);
//Sort
allFruits.sort();
document.write("<br><br>Использован метод sort: <br>" + allFruits);
//Push
var newLength = allFruits.push("мандарин");
document.write("<br><br>Использован метод push: <br>" + allFruits);
document.write("<br>Новая длина после push - " + newLength);
//Pop
var deletedElement = allFruits.pop();
document.write("<br><br>Использован метод pop: <br>" + allFruits);
document.write("<br>Значение удаленное с помощью pop - " + deletedElement);
//Unshift
var unshiftLength = allFruits.unshift("мандарин");
document.write("<br><br>Использован метод unshift: <br>" + allFruits);
document.write("<br>Новая длина после unshift - " + unshiftLength);
//Shift
var shiftDeleted = allFruits.shift();
document.write("<br><br>Использован метод shift: <br>" + allFruits);
document.write("<br>Значение удаленное с помощью shift - " + shiftDeleted);