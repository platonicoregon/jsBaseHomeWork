var max = -Infinity;
var min = Infinity;
var arr = [0, 1, 6, 142, 8, 5, 8, -5, -14, 9, 54, 79, 55, 126, 305];

for(var i = 0; i < arr.length; i++) {
	if(arr[i] > max) {
		max = arr[i];
	}
	if(arr[i] < min) {
		min = arr[i];
	}
}

console.log("Максимальное значение массива = " + max);
console.log("Минимальное значение массива = " + min);