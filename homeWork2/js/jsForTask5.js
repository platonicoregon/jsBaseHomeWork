var arr = [1, 2, 3, 4, 5, 6, 7, 8];
var afterElement;
var newElement;
var newArr = arr.slice();

do {
	afterElement = prompt("Массив [" + newArr + "]. Введите элемент массива после которого вставить новый элемент");
	for(var i = 0; i < newArr.length; i++) {
		if(newArr[i] == afterElement) {
			newElement = prompt("Введите значение нового элемента");
			if(newElement == "" || newElement == null) {
				alert("Вы не ввели значение нового элемента");
			}
			else {
				newArr.splice(i+1, 0, newElement);
				continue;
			}
		}
	}
}while(afterElement != null);

document.write("Изначальный массив: [" + arr + "]<br>");
document.write("Массив с добавленными вами элементами: [" + newArr + "]");