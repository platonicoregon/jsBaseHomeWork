function arrLength(arr) {
	if(arr == undefined){
		alert("Ошибка");
	}
	else {
		var l = 0;
		while (arr[l] != undefined) {
			l++;
		}
		return l;
	}
}

var defaultOrNot = confirm("Хотите сами ввести элементы массива?");

if(defaultOrNot == true) {
	var yourArr = new Array();
	var i = 0;
	do {
		yourArr[i] = prompt("Введите элемент массива №" + i);
		i++
	}
	while(yourArr[i - 1] != null && yourArr[i - 1] != "");
	yourArr.pop(); //удаление последнего элемента со значением "" или null 
	document.write("Ваш массив [" + yourArr + "]<br>");
	document.write("Его длина = " + arrLength(yourArr));
}
else {
	var yourArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	document.write("Массив со стандартными значениями [" + yourArr + "]<br>");
	document.write("Его длина = " + arrLength(yourArr));
};