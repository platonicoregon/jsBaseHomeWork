function fibanachiNumb(n) {
	var elementN = 0;
	var a = 1;
	var b = 0;
	var i = 1;
	while(i < n) {
		b = elementN + a;
		elementN = a;
		a = b;
		i++;
	}
	return elementN;
}

do {
	var c = +prompt("Какой элемент из числовой последовательности Фибоначчи вывести?");
}
while(isNaN(c) || c <= 0);

alert("Элемент числовой последовательности Фибоначчи №" + c + " = " + fibanachiNumb(c));