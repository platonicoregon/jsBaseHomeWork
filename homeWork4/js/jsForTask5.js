var workers = new Array();
var i = 0;
var next;
while(next != false) {
	var worker = new Object();
	worker = {
		name: prompt("Введите имя сотрудника"),
		sName: prompt("Введите фамилию сотрудника"),
		age: prompt("Введите возраст сотрудника"),
		occupation: prompt("Введите должность сотрудника"),
		show: function() {
			document.write("Имя: " + this.name + " <br>");
			document.write("Фамилия: " + this.sName + " <br>");
			document.write("Возраст: " + this.age + " <br>");
			document.write("Должность: " + this.occupation + " <br><br>");
		}
	}
	if(worker.name == null || worker.name == "" || worker.sName == null || worker.sName == "" || worker.age == null || worker.age == "" || worker.occupation == null || worker.occupation == "") {
		alert("Вы ввели не все данные о сотруднике");
	}
	else {
		workers[i] = worker;
		i++;
	}
	next = confirm("Добавить сотрудника?");
}

function addSalary (arr) {
	for(i = 0; i < arr.length; i++) {
		if(arr[i].occupation == "director") {
			arr[i].salary = 3000;
		}
		else if(arr[i].occupation == "manager") {
			arr[i].salary = 1500;
		}
		else if(arr[i].occupation == "programmer") {
			arr[i].salary = 2000;
		}
		else {
			arr[i].salary = 1000;
		}
		arr[i].show = function() {
			document.write("Имя: " + this.name + " <br>");
			document.write("Фамилия: " + this.sName + " <br>");
			document.write("Возраст: " + this.age + " <br>");
			document.write("Должность: " + this.occupation + " <br>");
			document.write("Зарплата: " + this.salary + " <br><br>");
		}
	}
}
function sortWorkers(arr) {
	do {
		var sortProp = prompt("По какому свойству отсортировать массив сотрудников? (name, sName, age, occupation, salary)");
	}
	while(sortProp != "name" && sortProp != "sName" && sortProp != "age" && sortProp != "occupation" && sortProp != "salary");
	if(sortProp == "name" || sortProp == "sName" || sortProp == "occupation") {
		arr.sort(function (a, b) {
			if (a[sortProp] > b[sortProp]) {
				return 1;
			}
			if (a[sortProp] < b[sortProp]) {
				return -1;
			}
			return 0;
		});
	}
	else {
		arr.sort(function(a, b) {return a[sortProp] - b[sortProp]});
	}
}

addSalary(workers);
sortWorkers(workers);

if(workers.length == 0) {
	alert("У вас нет сотрудников");
}
else {
	for(i = 0; i < workers.length; i++){
	document.write("Сотрудник №" + (i + 1) + ": <br>");
	workers[i].show();
	}
}