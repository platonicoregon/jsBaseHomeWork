var workers = new Array();
var i = 0;
var next;
while(next != false) {
	var worker = new Object();
	worker = {
		name: prompt("Введите имя сотрудника"),
		sName: prompt("Введите фамилию сотрудника"),
		age: prompt("Введите возраст сотрудника"),
		occupation: prompt("Введите должность сотрудника"),
		show: function() {
			document.write("Имя: " + this.name + " <br>");
			document.write("Фамилия: " + this.sName + " <br>");
			document.write("Возраст: " + this.age + " <br>");
			document.write("Должность: " + this.occupation + " <br><br>");
		}
	}
	if(worker.name == null || worker.name == "" || worker.sName == null || worker.sName == "" || worker.age == null || worker.age == "" || worker.occupation == null || worker.occupation == "") {
		alert("Вы ввели не все данные о сотруднике");
	}
	else {
		workers[i] = worker;
		i++;
	}
	next = confirm("Добавить сотрудника?");
}

if(workers.length == 0) {
	alert("У вас нет сотрудников");
}
else {
	for(i = 0; i < workers.length; i++){
	document.write("Сотрудник №" + (i + 1) + ": <br>");
	workers[i].show();
	}
}